package com.joaocruz04.githubrepositories

import com.joaocruz04.githubrepositories.data.network.GithubAPI
import org.junit.Test
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import org.assertj.core.api.Assertions.assertThat
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class SearchRepositoriesIntegrationTest {

    companion object {

        private const val GITHUB_SEARCH_RESULTS = "github_search_results.json"

    }

    private val mockWebServer = MockWebServer()

    private lateinit var testedClass: GithubAPI

    @Before
    fun setUp() {
        mockWebServer.start()
        testedClass = provideGitHubApi()
    }

    @Test
    fun `Given query, when requesting repositories with that query, then a list of repositories is returned`() {
        val query = "ANDROID"
        mockWebServer.enqueue(MockResponse().setBody(provideJsonFromFile(GITHUB_SEARCH_RESULTS)))

        val response = testedClass.repositories(query).blockingGet()

        assertThat(response.total_count).isEqualTo(724504L)
        assertThat(response.items.size).isEqualTo(30)
        assertThat(response.items[0].id).isEqualTo(12544093L)
        assertThat(response.items[0].name).isEqualTo("Android")
        assertThat(response.items[0].owner.id).isEqualTo(3790597L)
        assertThat(response.items[0].owner.login).isEqualTo("hmkcode")
        assertThat(response.items[0].owner.avatar_url).isEqualTo("https://avatars3.githubusercontent.com/u/3790597?v=4")
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    /* HELPERS */

    private fun provideGitHubApi(): GithubAPI {
        val retrofit = Retrofit.Builder()
                .baseUrl(mockWebServer.url("https://api.github.com"))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        return retrofit.create(GithubAPI::class.java)
    }

    private fun provideJsonFromFile(fileName: String): String {
        val resource = this::class.java.classLoader.getResource(fileName)
        val inputStream = resource.openStream()
        val stringBuilder = StringBuilder()
        val bufferedReader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
        bufferedReader.use { stringBuilder.append(it.readText()) }
        return stringBuilder.toString()
    }
}
