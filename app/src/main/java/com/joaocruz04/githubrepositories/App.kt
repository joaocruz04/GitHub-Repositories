package com.joaocruz04.githubrepositories

import com.joaocruz04.githubrepositories.dagger.DaggerAppComponent
import dagger.android.support.DaggerApplication

class App: DaggerApplication() {

    override fun applicationInjector() = DaggerAppComponent.builder()
            .application(this)
            .build()

}