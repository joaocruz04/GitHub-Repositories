package com.joaocruz04.githubrepositories.data.repository

import com.joaocruz04.githubrepositories.data.network.GithubAPI
import com.joaocruz04.githubrepositories.domain.model.SearchResult
import com.joaocruz04.githubrepositories.domain.repository.RepositoriesRepository
import io.reactivex.Single
import javax.inject.Inject

class RepositoriesRepositoryImpl @Inject constructor(private val githubAPI: GithubAPI) : RepositoriesRepository {

    override fun repositories(query: String): Single<SearchResult> {
        return githubAPI.repositories(query)
    }
}