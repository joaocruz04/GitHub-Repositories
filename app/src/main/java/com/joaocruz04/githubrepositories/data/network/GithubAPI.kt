package com.joaocruz04.githubrepositories.data.network

import com.joaocruz04.githubrepositories.domain.model.SearchResult
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubAPI {

    @GET("search/repositories")
    fun repositories(@Query("q") query: String): Single<SearchResult>

}