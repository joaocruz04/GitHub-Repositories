package com.joaocruz04.githubrepositories.dagger

import com.joaocruz04.githubrepositories.domain.rx.RxSchedulers
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
class SchedulersModule {

    @Provides
    fun providesSchedulers() = RxSchedulers(Schedulers.io(), AndroidSchedulers.mainThread())

}