package com.joaocruz04.githubrepositories.dagger

import com.joaocruz04.githubrepositories.data.repository.RepositoriesRepositoryImpl
import com.joaocruz04.githubrepositories.domain.repository.RepositoriesRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoriesModule {

    @Binds
    abstract fun bindCredentialsRepository(repositoriesRepository: RepositoriesRepositoryImpl):
            RepositoriesRepository

}