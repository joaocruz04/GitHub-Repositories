package com.joaocruz04.githubrepositories.dagger

import android.arch.lifecycle.ViewModelProvider
import com.joaocruz04.githubrepositories.presentation.search.MainActivity
import com.joaocruz04.githubrepositories.presentation.search.MainActivitySubModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [(MainActivitySubModule::class)])
    abstract fun bindsMainActivity(): MainActivity

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}