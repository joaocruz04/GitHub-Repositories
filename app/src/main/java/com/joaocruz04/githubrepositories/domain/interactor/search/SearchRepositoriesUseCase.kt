package com.joaocruz04.githubrepositories.domain.interactor.search

import com.joaocruz04.githubrepositories.domain.interactor.UseCaseSingle
import com.joaocruz04.githubrepositories.domain.model.SearchResult
import com.joaocruz04.githubrepositories.domain.repository.RepositoriesRepository
import com.joaocruz04.githubrepositories.domain.rx.RxSchedulers
import io.reactivex.Single
import javax.inject.Inject

open class SearchRepositoriesUseCase @Inject constructor(
        private val repository: RepositoriesRepository,
        rxSchedulers: RxSchedulers
): UseCaseSingle<SearchResult, SearchRepositoriesUseCase.Params>(rxSchedulers) {


    override fun buildUseCaseObservable(params: Params): Single<SearchResult> {
        return repository.repositories(params.query)
    }


    class Params(val query: String)

}