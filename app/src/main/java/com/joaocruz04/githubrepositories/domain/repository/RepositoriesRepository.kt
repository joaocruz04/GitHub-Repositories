package com.joaocruz04.githubrepositories.domain.repository

import com.joaocruz04.githubrepositories.domain.model.SearchResult
import io.reactivex.Single

interface RepositoriesRepository {

    fun repositories(query: String): Single<SearchResult>

}