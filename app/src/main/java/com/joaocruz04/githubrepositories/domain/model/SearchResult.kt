package com.joaocruz04.githubrepositories.domain.model

data class SearchResult(var total_count: Long, var items: ArrayList<GithubRepository>) {

    companion object {

        fun empty() = SearchResult(0, ArrayList())

    }

}
