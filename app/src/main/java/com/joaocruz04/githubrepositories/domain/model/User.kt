package com.joaocruz04.githubrepositories.domain.model

data class User (
        val id: Long,
        val login: String,
        val avatar_url: String
)