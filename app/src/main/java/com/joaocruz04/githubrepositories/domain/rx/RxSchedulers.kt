package com.joaocruz04.githubrepositories.domain.rx

import io.reactivex.Scheduler

class RxSchedulers(val io: Scheduler, val androidMainThread: Scheduler)
