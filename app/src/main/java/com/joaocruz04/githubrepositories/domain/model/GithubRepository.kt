package com.joaocruz04.githubrepositories.domain.model

data class GithubRepository(
        val id: Long,
        val name: String,
        val full_name: String,
        val owner: User,
        val description: String?,
        val forks: Long
)
