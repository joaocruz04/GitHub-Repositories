package com.joaocruz04.githubrepositories.presentation.search.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.joaocruz04.githubrepositories.R
import com.joaocruz04.githubrepositories.domain.model.GithubRepository

class RepositoriesAdapter(private val items: ArrayList<GithubRepository> = ArrayList()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = View.inflate(parent.context, R.layout.repository_item, null)
        return RepositoryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RepositoryViewHolder).bind(items[position])
    }
}
