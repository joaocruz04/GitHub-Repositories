package com.joaocruz04.githubrepositories.presentation.search

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.joaocruz04.githubrepositories.domain.interactor.search.SearchRepositoriesUseCase
import com.joaocruz04.githubrepositories.domain.model.SearchResult
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(private val searchUseCase: SearchRepositoriesUseCase): ViewModel() {

    val searchResultsLiveData = MutableLiveData<SearchResult>()
    val loadingProgressBarLiveData = MutableLiveData<Boolean>()

    fun searchRepositories(query: String) {
        if (query.isEmpty()) {
            publishResult(SearchResult.empty())
        } else {
            loadingProgressBarLiveData.postValue(true)
            searchUseCase.execute(SearchRepositoriesSubscriber(), SearchRepositoriesUseCase.Params(query))
        }
    }

    inner class SearchRepositoriesSubscriber : DisposableSingleObserver<SearchResult>() {

        override fun onSuccess(results: SearchResult) {
            publishResult(results)
        }

        override fun onError(exception: Throwable) {
            publishResult(null)
        }

    }

    private fun publishResult(searchResult: SearchResult?) {
        searchResultsLiveData.postValue(searchResult)
        loadingProgressBarLiveData.postValue(false)
    }

}