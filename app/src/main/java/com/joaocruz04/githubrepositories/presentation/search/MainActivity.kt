package com.joaocruz04.githubrepositories.presentation.search

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.joaocruz04.githubrepositories.presentation.BaseActivity
import com.joaocruz04.githubrepositories.R
import com.joaocruz04.githubrepositories.presentation.search.recyclerview.RepositoriesAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : BaseActivity<MainActivityViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObservers()
        initSearchField()
        updateLoadingStatus(false)
    }

    private fun initObservers() {
        viewModel.searchResultsLiveData.observe(this, Observer {
            it?.let {
                repositories_rv.adapter = RepositoriesAdapter(it.items)
            }
        })

        viewModel.loadingProgressBarLiveData.observe(this, Observer {
            it?.let { updateLoadingStatus(it) }
        })
    }

    private fun initSearchField() {
        search_et.addTextChangedListener(object: TextWatcher {
            private var timer = Timer()
            private val DELAY = 1000L

            override fun afterTextChanged(s: Editable?) {
                timer.cancel()
                timer = Timer()
                timer.schedule(object: TimerTask() {
                    override fun run() {
                        s?.let { viewModel.searchRepositories(it.toString()) }
                    }

                }, DELAY)
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun updateLoadingStatus(isLoading: Boolean) {
        loading_pb.visibility = if (isLoading) View.VISIBLE else View.GONE
        repositories_rv.alpha = if (isLoading) 0.5F else 1.0F
    }

    override fun constructViewModel(): Class<MainActivityViewModel> {
        return MainActivityViewModel::class.java
    }
}
