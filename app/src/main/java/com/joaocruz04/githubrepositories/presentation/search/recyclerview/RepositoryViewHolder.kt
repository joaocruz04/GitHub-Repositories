package com.joaocruz04.githubrepositories.presentation.search.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.joaocruz04.githubrepositories.R
import com.joaocruz04.githubrepositories.domain.model.GithubRepository
import kotlinx.android.synthetic.main.repository_item.view.*

class RepositoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(githubRepository: GithubRepository) {
        bindAvatar(githubRepository.owner.avatar_url)
        bindName(githubRepository.name)
        bindDescription(githubRepository.description)
        bindForkCount(githubRepository.forks)
    }

    private fun bindAvatar(avatarUrl: String?) {
        Glide.with(itemView.context)
                .load(avatarUrl)
                .apply(RequestOptions().placeholder(R.drawable.user_avatar_placeholder))
                .into(itemView.owner_avatar_iv)
    }

    private fun bindName(name: String) {
        itemView.repository_name_tv.text = name
    }

    private fun bindDescription(description: String?) {
        description?.let {
            itemView.repository_description_tv.text = description
            itemView.repository_description_tv.visibility = if (it.isEmpty()) View.GONE else View.VISIBLE
        }?:let {
            itemView.repository_description_tv.visibility = View.GONE
        }
    }

    private fun bindForkCount(forkCount: Long) {
        val forkLblText = itemView.context.getString(R.string.forks_label)
        itemView.repository_forks_tv.text = "$forkLblText $forkCount"
    }

}
