package com.joaocruz04.githubrepositories.presentation.search

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.joaocruz04.githubrepositories.domain.interactor.search.SearchRepositoriesUseCase
import com.joaocruz04.githubrepositories.domain.model.SearchResult
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainActivityViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val searchResultObserverMock = mock<Observer<SearchResult>>()
    private val loadingProgressBarObserverMock = mock<Observer<Boolean>>()

    private lateinit var searchRepositoriesUseCaseMock:SearchRepositoriesUseCase

    private lateinit var testedClass: MainActivityViewModel
    private lateinit var testedSubscriber: MainActivityViewModel.SearchRepositoriesSubscriber

    @Before
    fun setUp() {
        searchRepositoriesUseCaseMock = mock()
        testedClass = MainActivityViewModel(searchRepositoriesUseCaseMock)
        testedClass.searchResultsLiveData.observeForever(searchResultObserverMock)
        testedClass.loadingProgressBarLiveData.observeForever(loadingProgressBarObserverMock)
        testedSubscriber = testedClass.SearchRepositoriesSubscriber()
    }

    @Test
    fun `Given query, when user searches for repository, then ui gets notified to show loading spinner`() {
        val query = "ANDROID"

        testedClass.searchRepositories(query)

        verify(loadingProgressBarObserverMock).onChanged(true)
        verify(searchRepositoriesUseCaseMock).execute(any(), any())
    }

    @Test
    fun `Given no query, when user searches for repository, then ui gets notified not to show loading spinner`() {
        testedClass.searchRepositories("")

        verify(loadingProgressBarObserverMock).onChanged(false)
        verifyZeroInteractions(searchRepositoriesUseCaseMock)
    }


    @Test
    fun `When service returns a search result, then ui gets notified with new repositories`() {
        val searchResult = SearchResult(123, arrayListOf())

        testedSubscriber.onSuccess(searchResult)

        verify(loadingProgressBarObserverMock).onChanged(false)
        verify(searchResultObserverMock).onChanged(searchResult)
    }

    @Test
    fun `When service returns an error, then ui gets notified with no new repositories`() {
        testedSubscriber.onError(Throwable())

        verify(loadingProgressBarObserverMock).onChanged(false)
        verify(searchResultObserverMock).onChanged(null)
    }
}